import {useEffect, useState} from "react";
import {Link} from "react-router-dom";

function ListSales() {
  const [sales, setSales] = useState([]);
  const [filteredSales, setFilteredSales] = useState([]);
  const [salespersons, setSalespersons] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState("");

  const loadSales = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");

    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
      setFilteredSales(data.sales); // Initially all of the sales
      console.log(data);
    }
  };

  const loadSalespersons = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    console.log(response)

    if (response.ok) {
      const dataSP = await response.json();
      console.log(dataSP)
      setSalespersons(dataSP.salespersons);
    }
  };

  useEffect(() => {
    loadSales();
    loadSalespersons();
  }, []);

  const handleSPChange = (e) => {
    const selectedSalespersonId = Number(e.target.value);
    // console.log("SPerson ID: ", selectedSalespersonId);
    // console.log(typeof(selectedSalespersonId));
    setSelectedSalesperson(selectedSalespersonId);
    if (selectedSalespersonId === "") {
      setFilteredSales(sales);
    } else {
      const filtered = sales.filter((sale) => {
        // console.log("SP ID :", sale.salesperson.employee_id);
        // console.log(typeof(sale.salespersons.employee_id));
        return sale.salesperson.employee_id === selectedSalespersonId;
        });
      console.log(filtered);
      setFilteredSales(filtered);
    }
  };

  return (
    <div className="list-sales-container">
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <select
              className="form-control"
              value={selectedSalesperson}
              onChange={handleSPChange}>
              <option value="">All Salespersons</option>
              {salespersons.map((salesperson) => (
                <option key={salesperson.employee_id} value={salesperson.employee_id}>
                  {salesperson.first_name} {salesperson.last_name}
                </option>
              ))}
            </select>
          </div>
        </div>
        <div className="row">
          {filteredSales.map((sale) => {
            const vin = sale.automobile.vin;
            return (
              <div key={sale.id} className="col-md-4 mb-4">
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">{sale.automobile.year} {sale.automobile.manufacturer} {sale.automobile.model}</h5>
                    <h6 className="card-text">${sale.price}</h6>
                    <img src={sale.automobile.picture_url} className="card-img-top" alt={sale.automobile.color}></img>
                    <p className="small text-muted">VIN: {sale.automobile.vin}</p>
                    <p className="card-text">
                      Buyer: {sale.customer.last_name}, {sale.customer.first_name}
                    </p>
                    <p className="card-text">
                      Salesperson: {sale.salesperson.first_name} {sale.salesperson.last_name} {""}
                      <span className="small text-muted">ID: {sale.salesperson.employee_id}</span>
                    </p>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default ListSales;
