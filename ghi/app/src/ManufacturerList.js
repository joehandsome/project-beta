import React, { useState, useEffect } from 'react';


function ManufacturerList() {
    const [manufacturers, setManufactuers] = useState([]);

    const getManufacturers = async function() {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        const manufacturersOb = await response.json();
        setManufactuers(manufacturersOb.manufacturers);
    }

    useEffect(() => {
        getManufacturers();
    }, []);


    return (

        <div>
            <h2 className="text-center mb-3 p-3">Registered Manufacturers</h2>
            <div className="border bg-light">
                {manufacturers.map((manufacturer) => {
                    return <div className="p-3 text-center border-bottom border-success" key={manufacturer.id}>{manufacturer.name}</div>
                })}
            </div>
        </div>

    );


}


export default ManufacturerList;
