import React, {useState, useEffect} from 'react';


const CreateAutomobile = function() {

    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [model,setModel] = useState('');
    const [models, setModels] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setModels(data.models);
            console.log(data);
        }
    }

    console.log("models:", models);

    useEffect(() => {
        fetchData();
    }, []);

    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleChangeYear = (event) => {
        const value = event.target.value;
        setYear(value);
    }

    const handleChangeVin = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleChangeModel = (event) => {
        const value = event.target.value;
        setModel(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.color = color;
        data.year = Number(year);
        data.vin = vin;
        data.model_id = Number(model);

        const url = "http://localhost:8100/api/automobiles/"
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const autoResponse = await fetch(url, fetchOptions);
        if (autoResponse.ok){
            const newAuto = await autoResponse.json();

            setColor('');
            setYear('');
            setVin('');
            setModel('');
        };
    };

    return (
        <div className="p-3 mb-2 bg-light text-emphasis-dark">
            <form onSubmit={handleSubmit} id="create-auto-form">
                <h2>Register a Vehicle</h2>
                <div className="mb-3"/>
                <div className="row g-3">
                    <div className="form-floating col-sm-7">
                        <input className="form-control form-control-lg mb-3" onChange={handleChangeVin} placeholder="vehicle VIN" value={vin} type="text" name="vin" id="vin" />
                        <label htmlFor="vin">VIN</label>
                    </div>
                    <div className="form-floating col-sm">
                        <input className="form-control form-control-lg mb-3" onChange={handleChangeColor} placeholder="paint color" type="text" value={color} name="color" id="color" />
                        <label htmlFor="color">Paint Color</label>
                    </div>
                    <div className="form-floating col-sm">
                        <input className="form-control form-control-lg mb-3" onChange={handleChangeYear} placeholder="year" value={year} type="text" name="year" id="year" />
                        <label htmlFor="year">Year</label>
                    </div>
                </div>

                    <div className="input-group input-group-lg mb-3">
                        <select className="form-select mb-3" onChange={handleChangeModel} value={model} id="model" name="model">
                            <option value="">Select Model</option>
                            {models.map(model => {
                                return (
                                    <option key={model.id} value={model.id}>{model.name}</option>
                                )
                            })}
                        </select>
                        <button className="btn btn btn-success" data-bs-toggle="modal" data-bs-target="#vehicleSubmitModal" type="button">Submit</button>
                        <div className="modal fade" id="vehicleSubmitModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div className="modal-dialog">
                                <div className="modal-content">
                                <div className="modal-header">
                                    <h1 className="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
                                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div className="modal-body">
                                    {color} is a great color! Your car is saved and ready to go!
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={handleSubmit}>Confirm</button>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    );
}




{/* <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div> */}

export default CreateAutomobile;
