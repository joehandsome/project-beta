import React, { useEffect, useState } from 'react';


const AppointmentList = function(){
    const [appointments, setAppointments] = useState([]);

    const getAppointments = async function() {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        console.log("appointments:", response);
        const appointmentsOb = await response.json();
        const appointments = appointmentsOb["appointments"];
        let createdAppt = []
        for (let a of appointments){
            if (a.status == "created"){
                createdAppt.push(a)
            }
        }
        setAppointments(createdAppt)
    }

    useEffect(() => {
            getAppointments();
        }, [])

    function checkVIP(value){
        if (value == true){return "VIP"}
        else if (value == false){return "not VIP"}
        else {return "unsure"}
    }

    const handleCancel = async function(appt){
        console.log("canceling appt for ", appt.customer);
        const url = `http://localhost:8080/api/appointments/${appt.id}/`;
        const cancelString = {"status": "canceled"}
        const fetchOptions = {
            method: 'put',
            body: JSON.stringify(cancelString),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const cancelResponse = await fetch(url, fetchOptions);
        if (cancelResponse.ok) {
            console.log(`appointment for ${appt.customer} has been canceled`)
            getAppointments();
        }
    }

    const handleComplete = async function(appt){
        console.log("completing appt for ", appt.customer)
        const url = `http://localhost:8080/api/appointments/${appt.id}/`
        const completeString = {"status": "completed"}
        const fetchOptions = {
            method: "put",
            body: JSON.stringify(completeString),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const completeResponse = await fetch(url, fetchOptions);
        if (completeResponse.ok) {
            console.log(`appointment for ${appt.customer} has been completed`)
            getAppointments();
        }
    }


    return(
        <div className="App">
            <h2 className="p-3 mb-3 text-center">Active Appointments</h2>
            <table className="p-3 mb-3 container text-center ">
                <thead className="bg-success text-light">
                    <tr>
                        <th>VIN</th>
                        <th>VIP</th>
                        <th>Cusomter</th>
                        <th>Date</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
            {appointments.map((appointment) => {
                return <tbody key={appointment.id}>
                        <tr className="mb-3 border border-success bg-light form-floating ">

                            <td>{appointment.vin}</td>
                            <td>{checkVIP(appointment.vip)}</td>
                            <td>{appointment.customer}</td>
                            <td>{appointment.date_time}</td>
                            <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                            <td>{appointment.reason}</td>
                            <td>
                                <button className="btn btn-danger" onClick={() => handleCancel(appointment)}>Cancel</button>
                            </td>
                            <td>
                                <button className="btn btn-success" onClick={() => handleComplete(appointment)}>Complete</button>
                            </td>
                    </tr>
                </tbody>
            })
            }
            </table>
        </div>
    );
}


export default AppointmentList;
