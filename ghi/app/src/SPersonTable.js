import {useEffect, useState} from 'react';
import {Link} from "react-router-dom";

function SPersonTable() {
  const [sPerson, setSPerson] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/salespeople/');

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setSPerson(data.salespersons);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {sPerson.map(sPerson => {
            return (
              <tr key={sPerson.employee_id}>
                <td>{sPerson.first_name} {sPerson.last_name}</td>
                <td>{sPerson.employee_id}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Link to="/salespeople/new" className="btn btn-primary fixed-bottom">
        Add a Salesperson
      </Link>
    </div>
  );
}

export default SPersonTable;
