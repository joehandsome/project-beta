import React, {useState, useEffect } from 'react';


const CreateAppointment = function(){

    const [vin, setVin] = useState('');
    const [customer, setCustumer] = useState('');
    const [date, setDate] = useState('');
    const [technician, setTechnician] = useState('');
    const [technicians, setTechnicians] = useState([]);
    const [reason, setReason] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            console.log("here is my data:", data);
            setTechnicians(data.technicians);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustumer(value);
    }

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date_time = date;
        data.technician = technician;
        data.reason = reason;

        const url = "http://localhost:8080/api/appointments/";
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const appointmnetResponse = await fetch(url, fetchOptions);
        if (appointmnetResponse.ok) {
            const newAppointment = await appointmnetResponse.json();
            console.log(newAppointment);

            setVin('');
            setCustumer('');
            setDate('');
            setTechnician('');
            setReason('');
        }
    }

    console.log("technicians:", technicians)
    return(

        <div className="p-3 mb-2 bg-light text-emphasis-dark">
            <div>
                <h2 className="mb-3">Make an Appointment!</h2>
            </div>
            <form id="create-application-form">
                <div className="row g-3">
                    <div className="form-floating col-sm">
                        <input className="form-control form-control-lg mb-3" onChange={handleVinChange} placeholder="put VIN here" value={vin} type="text" name="vin" id="vin"/>
                        <label htmlFor="vin">VIN</label>
                    </div>
                    <div className="form-floating col-sm">
                        <input className="form-control form-control-lg mb-3" onChange={handleCustomerChange} placeholder="your name here" value={customer} type="text" name="customer" id="customer"/>
                        <label htmlFor="customer">customer</label>
                    </div>
                    <div className="form-floating col-sm">
                        <input className="form-control form-control-lg mb-3" onChange={handleDateChange} placeholder="When would you like your appointment?" value={date} type="date" name="date" id="date" />
                        <label htmlFor="date">Date</label>
                    </div>
                </div>
                <div>
                    <select className="form-select form-select-lg mb-3" onChange={handleTechnicianChange} value={technician} id="technician" name="technician">
                        <option value="">Select a wonderful technician</option>
                        {technicians.map(technician => {
                            return (
                                <option key={technician.id} value={technician.employee_id}>{technician.first_name} {technician.last_name}</option>
                            )
                        })}
                    </select>
                </div>
                <div className="input-group input-group-lg mb-3">
                    <div className="form-floating col-sm">
                        <textarea className="form-control form-control-lg mb-3" onChange={handleReasonChange} placeholder="what brings you in today?" value={reason} type="text" name="reason" id="reason" />
                        <label htmlFor="reason">Reason</label>
                    </div>
                    <button type="button" className="btn btn-success" data-bs-toggle="modal" data-bs-target="#appointmentSubmitModal">Create</button>
                    <div className="modal fade" id="appointmentSubmitModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div className="modal-dialog">
                                <div className="modal-content">
                                <div className="modal-header">
                                    <h1 className="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
                                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div className="modal-body">
                                    Thanks {customer}, your appointment is confirmed!
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={handleSubmit}>Confirm</button>
                                </div>
                                </div>
                            </div>
                        </div>
                </div>
            </form>
        </div>

    );
}


export default CreateAppointment;
