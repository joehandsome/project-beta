import React, { useState, useEffect } from 'react';


function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [VIN, setVIN] = useState('');

    const getAppointments = async function() {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        const appointmentsOb = await response.json();
        const appointments = appointmentsOb["appointments"];
        setAppointments(appointments)
        console.log("array??", Array.isArray(appointments), appointments)

    }

    useEffect(() => {
            getAppointments();
        }, [])

    function checkVIP(value){
        if (value == true){return "VIP"}
        else if (value == false){return "not VIP"}
        else {return "unsure"}
    }

    const handleVINSearch = (event) => {
        const value = event.target.value;
        setVIN(value);
    }

    const handleVINSubmit = async function(v) {
        const matchingVin = [];
        for (let a of appointments){
            if (a.vin == v){
                console.log(a)
                matchingVin.push(a)
                console.log(matchingVin)
            }
        }
        console.log(matchingVin)
        setAppointments(matchingVin)
    }

    const handleVINReset = async function() {
        console.log("resetting appointment list")
        getAppointments();
    }

    return(
        <div>
            <div className="p-3 mb-1"></div>
            <div className="mb-3 text-center input-group">
                <div className="form-floating">
                    <input className="form-control" onChange={handleVINSearch} placeholder="VIN here" value={VIN} type="text" name="VIN" id="VIN" />
                    <label htmlFor="VIN">VIN</label>
                </div>
                <button className="btn btn-success" onClick={() => handleVINSubmit(VIN)}>Search</button>
                <button className="btn btn-secondary" onClick={() => handleVINReset()}>Reset</button>
            </div>
            <h1 className="mb-3 text-center">Service Record</h1>
            <div>
                <table className="p-3 mb-3 bg-light container text-center">
                    <thead className="bg-success text-light">
                        <tr>
                            <th>VIN</th>
                            <th>VIP</th>
                            <th>Cusomter</th>
                            <th>Date</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                        {appointments.map((appointment) => {
                            return <tbody key={appointment.id}>
                                <tr className="border border-success rounded-pill">
                                    <td>{appointment.vin}</td>
                                    <td>{checkVIP(appointment.vip)}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{appointment.date_time}</td>
                                    <td>{appointment.technician.first_name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.status}</td>
                                </tr>
                            </tbody>
                        })
                        }
                </table>
            </div>
        </div>
    );
}


export default ServiceHistory;
