from django.urls import path
from .views import (
  api_list_salespersons,
  api_salesperson_details,
  api_list_customers,
  api_customer_details,
  api_list_sales,
  api_sale_details,
)

urlpatterns = [
  path("salespeople/", api_list_salespersons, name="list_salespersons"),
  path("salespeople/<int:pk>/", api_salesperson_details, name="salesperson_details"),
  path("customers/", api_list_customers, name="list_customers"),
  path("customers/<int:pk>/", api_customer_details, name="customer_details"),
  path("sales/", api_list_sales, name="list_sales"),
  path("sales/<int:pk>/", api_sale_details, name="sale_details"),
]
