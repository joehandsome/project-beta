from django.db import models
from django.core.validators import MinValueValidator

class AutomobileVO(models.Model):
  import_href = models.CharField(max_length=100, unique=True)
  color = models.CharField(max_length=50)
  year = models.PositiveSmallIntegerField()
  vin = models.CharField(max_length=17, unique=True)
  model = models.CharField(max_length=100)
  sold = models.BooleanField(default=False)
  picture_url = models.URLField(blank=True, null=True)
  manufacturer = models.CharField(max_length=100, blank=True, null=True)

class Salesperson(models.Model):
  first_name = models.CharField(max_length=20)
  last_name = models.CharField(max_length=20)
  employee_id = models.AutoField(primary_key=True)

class Customer(models.Model):
  first_name = models.CharField(max_length=20)
  last_name = models.CharField(max_length=20)
  address = models.CharField(max_length=100)
  phone_number = models.CharField(max_length=10)

class Sale(models.Model):
  price = models.IntegerField(
    validators = [
      MinValueValidator(1),
      # no losses in this dealership
    ]
  )
  salesperson = models.ForeignKey(
    Salesperson,
    related_name="sale",
    on_delete=models.PROTECT,
  )
  customer = models.ForeignKey(
    Customer,
    related_name="sale",
    on_delete=models.PROTECT,
  )
  # OneToOne for uniqueness
  automobile = models.OneToOneField(
    AutomobileVO,
    related_name="sale",
    on_delete=models.PROTECT,
    unique=True,
  )
