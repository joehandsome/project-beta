# CarCar
CarCar is an app for managing an auto dealership. It handles the inventory, sales, and repair services.

Team:

* Grayson - Service
* Johanson - Sales


## How to Run this App

1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone https://gitlab.com/joehandsome/project-beta

3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/

## Design
CarCar is made up of the following
- Inventory => Holds the info for the dealership's inventory, keeping track of all the details of an automobile.
- Sales => Covers the sales end of the dealership. This includes sales personnel, customer information, and sales records.
- Services => Keeps record of technicians, service appointments, and service records.

## Service microservice

The service microservice has 3 models:

-AutomobileVO  ==>  Uses a poller to copy data from inventory app. The "sold" property is set false by default, until the Sales microservice updates the corrosponding automobile model in inventory

-Technician  ==> holds technicians to be assigned to appointments

-Appointment  ==>  the most frontwardly facing and complex model, it takes a technician as a foreign key. On appointment creation, it will add a VIP property set to True if the VIN matches a VIN in AutomobileVO with sold:True, and set VIP property to False if there is no match, or a match with sold: False.

These can be accessed front-end through port

### Service Poller

	The service poller will update AutomobileVO database from the inventory database for model Automobiles with a GET request to "http://project-beta-inventory-api-1:8000/api/automobiles/". The poller will repeat every 60 seconds while the poller server is running in order to keep consitancy between models while Inventory's Automobile instances are edited and created. Print statements will appear in your terminal every loop to show that the poller is operating.
```

```


### URLs and Ports

	Service						URL

	React Webpage				http://localhost:3000/
	Inventory microservice		http://localhost:8100/
	Service microservice		http://localhost:8080/
	Sales microservice			http://localhost:8080/


### Inventory API (Optional)
 - Put Inventory API documentation here. This is optional if you have time, otherwise prioritize the other services.

### AutomobileVO

Action 					Method 			URL
List Automobile			GET				http://localhost:8080/api/autos/

List auto will return all autos with the vin and sold properties.
See the Appointment section for more details about creating automobiles and the sold property.

List Automobile will return
```
	{
		"autos": [
			{
				"vin": "ABCDEFGHIJKLMNOPQ",
				"sold": false
			},
			{
				"vin": "BCDEFGHIJKLMNOPQR",
				"sold": true
			}
		]
	}
```

 ### Technician

 Action					Method			URL

List Technician			GET				http://localhost:8080/api/technicians/
Create Technician		POST			http://localhost:8080/api/technicians/
Detail Technician		GET				http://localhost:8080/api/technicians/<<id>>/
Edit Technician			PUT				http://localhost:8080/api/technicians/<<id>>/
Delete Technician		DELETE			http://localhost:8080/api/technicians/<<id>>/

 Technician posts should be formatted:
 ```
	{
		"first_name": "Jane",
		"last_name": "Doe",
		"employee_id": "Tech_58"
	}
 ```

The response will include an id property:
 ```
	{
		"id": 11,
		"first_name": "Jane",
		"last_name": "Doe",
		"employee_id": "Tech_58"
	}
 ```

The id propery is not able to be updated.

List Technician and Detail Technician will both return all properties of the instance.


### Appointment

 Action					Method			URL

List Appointment		GET				http://localhost:8080/api/appointments/
Create Appointment		POST			http://localhost:8080/api/appointments/
Detail Appointment		GET				http://localhost:8080/api/appointments/<<id>>/
Edit Appointment		PUT				http://localhost:8080/api/appointments/<<id>>/
Delete Appointment		DELETE			http://localhost:8080/api/appointments/<<id>>/


Appointment posts should be formatted:
```
	{
		"date_time": "2025-12-14",
		"reason": "oil change",
		"vin": "ABCDEFGHIJKMNOPQ",
		"customer": "John Doe",
		"technician": "Tech_58"
	}
```
!vins should not be longer than 17 characters
!technicians should match the employee_id of an instance of the Technician model


The response will include additional properties id and vip, as well as the properties of the technician:
 ```
	{
		"id": 16,
		"status": "created",
		"customer": "John Doe",
		"technician": {
			"id": 6,
			"first_name": "Jane",
			"last_name": "Doe",
			"employee_id": "Tech_58"
		},
		"vin": "ABCDEFGHIJKMNOPQ",
		"reason": "oil change",
		"date_time": "2025-12-14",
		"vip": true
	}
 ```

How is vip determined?
	The value of the vip property is determined upon creation of the appointment instance based on the appointment.vin and list of AutomobileVO instances. Upon appointment creation, it will add a VIP property set to True if the VIN matches a VIN in AutomobileVO with sold:True, and set VIP property to False if there is no match, or a match with sold: False.


----------------------------------------------------------------------------



## Sales microservice

~~I'm just trying to make it work tbh.~~
The Sales microservice backend has 4 models:
- Customer
- Salesperson
- Sale
- AutomobileVO

AutomobileVO inherits its properties from the Automobile model in Inventory. To get that information, a poller was created. For a sale to be conducted, it would need a valid vehicle to be sold, and that information lives in the Inventory microservice.

All these services can be accessed on the frontend from port 3000.

## The Poller
```
---------------------|                                                            |-------------------
  sales-api      |poller.py|-"http://project-beta-api-1/:8000/api/automobiles"--->|     inventory-api
                     |                                                            |
         AutomobileVO|<-----{json containing automobile info}---------------------|Automobile
---------------------|                                                            |-------------------
```
## Customers
```
Action          Method          URL
List Customers  GET             http://localhost:8090/api/customers/
Add a Customer  POST            http://localhost:8090/api/customers/
Customer Detail GET             http://localhost:8090/api/customers/id/
```

JSON for adding a customer:
```
{
    "first_name": "Testy",
    "last_name": "McTestFace",
    "address": "123 House Place",
    "phone_number": "1123581321"
}
```

When a Customer is added, they are assigned an id. This is the return value after successfully adding a Customer:
```
{
    "id": 1,
    "first_name": "Testy",
    "last_name": "McTestFace",
    "address": "123 House Place",
    "phone_number": "1123581321"
}
```
When listing customers, an array of "customers" with each customer object containing the above information is returned.

## Salesperson
```
Action            Method          URL
List Salesperson  GET             http://localhost:8090/api/salespeople/
Add a Salesperson POST            http://localhost:8090/api/salespeople/
```

JSON for adding a Salesperson:
```
{
    "first_name": "Tester",
    "last_name": "McTesterFace"
}
```

When a Salesperson is added, they are assigned an employee_id. This is the return value after successfully adding a Salesperson:
```
{
    "first_name": "Tester",
    "last_name": "McTestFace",
    "employee_id": 1
}
```
When listing Salesperson, an array of "salespersons" with each salesperson object containing the above information is returned.


## Sale
```
Action            Method          URL
List Sales        GET             http://localhost:8090/api/sales/
Add a Sale        POST            http://localhost:8090/api/sales/
```

JSON for adding a Sale:
```
{
    "price": 5000,
    "customer": 1, ==> customer.id
    "salesperson": 1, ==> salesperson.employee_id
    "automobile": "/api/automobiles/JT2AE86S3E00000000/" ==> Takes the href of automobile
}
```

When a Sale is recorded, a PUT request is sent to the inventory-api in order to update the "sold" property of that vehicle to True.
Below is the return value after successfully adding a sale
```
{
	"id": 1,
	"price": 5000,
	"salesperson": {
		"first_name": "Tester",
		"last_name": "McTesterFace",
		"employee_id": 1
	},
	"customer": {
		"id": 1,
		"first_name": "Testy",
		"last_name": "McTestFace",
		"address": "123 House Place",
		"phone_number": "1123581321"
	},
	"automobile": {
		"import_href": "/api/automobiles/JT2AE86S3E0000001/",
		"color": "CF Green White Two-Tone",
		"year": 1984,
		"vin": "JT2AE86S3E0000001",
		"model": "Sprinter Trueno",
		"sold": true,
		"picture_url": "picture_url",
		"manufacturer": "Toyota"
	}
}
```
When listing Sales, an array of "sales" with each sale object containing the above information is returned.
