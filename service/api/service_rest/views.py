from django.shortcuts import render
from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment
from django.http import JsonResponse
import json

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold"
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
    ]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "status",
        "customer",
        "technician",
        "vin",
        "reason",
        "date_time",
        "vip"
    ]
    encoders = {
        "technician": TechnicianEncoder()
    }

def api_auto_list(request):

    if request.method == "GET":
        print("looking for autos")
        autos = AutomobileVO.objects.all()
        return JsonResponse(
            {"autos": autos},
            encoder=AutomobileVOEncoder,
        )

def api_technician_list(request):

    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "could not add technician to database :("}
            )
            response.status_code = 400
            return response

def api_technician_detail(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                {"technician": technician},
                encoder = TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Sorry, that technician does not exist!"})
            response.status_code = 404
            return response

    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.get(id=pk)
            props = [
                "first_name",
                "last_name"
                ]
            for prop in props:
                if prop in content:
                    setattr(technician, prop, content[prop])
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except:
            response = JsonResponse({"message": "oops, technician could not be updated"})
            response.status_code = 400
            return response

    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()
        if count < 1:
            response = JsonResponse({"message": "could not delete technician"})
            response.status_code = 404
            return response
        return JsonResponse({"deleted": count > 0})

def api_appointment_list(request):

    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            try:
                technician = Technician.objects.get(employee_id=content["technician"])
                print("this is the technician being searched for:", technician.first_name)
                content["technician"] = technician

            except Technician.DoesNotExist:
                return JsonResponse(
                    {"message": "Could not find technician"},
                    status=404
                )
            try:
                automobiles = AutomobileVO.objects.all()
                for automobile in automobiles:
                    vin = automobile.vin
                    print(automobile.vin, content["vin"], automobile.sold)
                    if content["vin"] == vin and automobile.sold == True:
                        content["vip"] = True
                    else:
                        content["vip"] = False

            except:
                return JsonResponse({"message": "something went wrong when checking VIP status"})
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False
            )
        except:
            response = JsonResponse({"message": "appointment could not be created"})
            response.status_code = 400
            return response

def api_appointment_detail(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                {"appointment": appointment},
                encoder = AppointmentListEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Could not find appointment, sowwy :("})
            response.status_code = 404
            return response

    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.get(id=pk)
            props = [
                "status",
                "customer",
                "technician",
                "vin",
                "reason",
                "date_time"
            ]
            for prop in props:
                if prop in content:
                    setattr(appointment, prop, content[prop])
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False
            )

        except:
            response = JsonResponse({"message": "could not update appointment"})
            response.status_code = 400
            return response

    elif request.method == "DELETE":

        count, _ = Appointment.objects.filter(id=pk).delete()
        if count < 1:
            response = JsonResponse({"message": "could not find appointment"})
            response.status_code = 404
            return response
        return JsonResponse({"deleted": count > 0})
