from django.contrib import admin
from .models import AutomobileVO, Technician, Appointment


@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = ["vin", "sold"]

@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    list_display = ["first_name", "last_name", "employee_id"]

@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = ["reason", "status", "vin",  "customer", "date_time", "technician"]
